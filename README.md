# Fortify SSC Parser Plugin for KICS

## Introduction

This Fortify SSC parser plugin allows for importing scan results from KICS.

### Related Links

* **Downloads**: https://gitlab.com/securitydojo-oss-dev/fortify-ssc-kics-parser/-/releases
* **Sample input files**: [sampleData](sampleData)
* **GitLab**: https://gitlab.com/securitydojo-oss-dev/fortify-ssc-kics-parser


## Plugin Installation

These sections describe how to install, upgrade and uninstall the plugin.

### Install & Upgrade

* Obtain the plugin binary jar file
* If you already have another version of the plugin installed, first uninstall the previously
  installed version of the plugin by following the steps under [Uninstall](#uninstall) below
* In Fortify Software Security Center:
    * Navigate to Administration->Plugins->Parsers
    * Click the `NEW` button
    * Accept the warning
    * Upload the plugin jar file
    * Enable the plugin by clicking the `ENABLE` button

### Uninstall

* In Fortify Software Security Center:
    * Navigate to Administration->Plugins->Parsers
    * Select the parser plugin that you want to uninstall
    * Click the `DISABLE` button
    * Click the `REMOVE` button


## Results format

Note that the SSC parser plugin requires the uploaded reports to be in JSON format  
JSON reports should looks like as following:
````
{
    "files_scanned": 2,
    "files_parsed": 2,
    "files_failed_to_scan": 0,
    "queries_total": 253,
    "queries_failed_to_execute": 0,
    "queries_failed_to_compute_similarity_id": 0,
    "queries": [
        {
            "query_name": "Container Allow Privilege Escalation Is True",
            "query_id": "c878abb4-cca5-4724-92b9-289be68bd47c",
            "severity": "MEDIUM",
            "platform": "Terraform",
            "files": [
                {
                    "file_name": "assets/queries/terraform/kubernetes/container_allow_privilege_escalation_is_true/test/positive.tf",
                    "similarity_id": "063ed2389809f5f01ff420b63634700a9545c5e5130a6506568f925cdb0f8e13",
                    "line": 11,
                    "issue_type": "IncorrectValue",
                    "search_key": "kubernetes_pod[test3].spec.container.allow_privilege_escalation",
                    "search_value": "",
                    "expected_value": "Attribute 'allow_privilege_escalation' is undefined or false",
                    "actual_value": "Attribute 'allow_privilege_escalation' is true",
                    "value": null
                }
            ]
        }
    ],
    "scan_id": "console",
    "severity_counters": {
        "HIGH": 0,
        "INFO": 0,
        "LOW": 0,
        "MEDIUM": 1
    },
    "total_counter": 1
}

````

## Upload results

As a 3<sup>rd</sup>-party results zip bundle:

* Generate a scan.info file containing a single line as follows:  
  `engineType=SECDOJO_KICS`
* Generate a zip file containing the following:
    * The scan.info file generated in the previous step
    * The JSON file containing scan results
* Upload the zip file generated in the previous step to SSC
    * Using any SSC client, for example FortifyClient or Maven plugin
    * Or using the SSC web interface
    * Similar to how you would upload an FPR file

## License

See [LICENSE.TXT](LICENSE.TXT)
