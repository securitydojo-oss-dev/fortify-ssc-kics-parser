package io.secdojo.ssc.parser.kics.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public final class File {
	@JsonProperty("file_name")
	private String name;
	@JsonProperty("similarity_id")
	private String similarity_id;
	@JsonProperty("line")
	private int line;
	@JsonProperty("issue_type")
	private String issue_type;
	@JsonProperty("search_key")
	private String search_key;
	@JsonProperty("search_line")
	private String search_line;
	@JsonProperty("search_value")
	private String search_value;
	@JsonProperty("expected_value")
	private String expected_value;
	@JsonProperty("actual_value")
	private String actual_value;
}
