package io.secdojo.ssc.parser.kics.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Query {

    @JsonProperty("query_name")
    private String name;
    @JsonProperty("query_id")
    private String id;
    @JsonProperty("query_url")
    private String query_url;
    @JsonProperty("severity")
    private String severity;
    @JsonProperty("platform")
    private String platform;
    @JsonProperty("cloud_provider")
    private String cloud_provider;
    @JsonProperty("category")
    private String category;
    @JsonProperty("description")
    private String description;
    @JsonProperty("description_id")
    private String description_id;
    @JsonProperty("files")
    private File[] files;
}
