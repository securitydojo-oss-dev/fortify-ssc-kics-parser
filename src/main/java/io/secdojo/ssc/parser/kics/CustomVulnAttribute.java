package io.secdojo.ssc.parser.kics;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {

    // Custom attributes must have their types defined:
    QueryUrl(AttrType.STRING),
    Platform(AttrType.STRING),
    CloudProvider(AttrType.STRING),
    Description(AttrType.LONG_STRING),
    FileName(AttrType.STRING),
    IssueType(AttrType.STRING),
    ExpectedValue(AttrType.STRING),
    QueryName(AttrType.STRING),
    ActualValue(AttrType.STRING);

    private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
