package io.secdojo.ssc.parser.kics.parser;

import java.io.*;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonToken;
import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.kics.CustomVulnAttribute;
import io.secdojo.ssc.parser.kics.domain.File;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.json.ScanDataStreamingJsonParser;
import io.secdojo.ssc.parser.kics.domain.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VulnerabilitiesParser {
    private static final Logger LOG = LoggerFactory.getLogger(VulnerabilitiesParser.class);

    private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();

    private static final Map<String, Priority> MAP_SEVERITY_TO_PRIORITY = Stream
            .of(new AbstractMap.SimpleImmutableEntry<>("INFO", Priority.Low),
                    new AbstractMap.SimpleImmutableEntry<>("LOW", Priority.Medium),
                    new AbstractMap.SimpleImmutableEntry<>("MEDIUM", Priority.High),
                    new AbstractMap.SimpleImmutableEntry<>("HIGH", Priority.Critical))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private final ScanData scanData;
    private final VulnerabilityHandler vulnerabilityHandler;

    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
        this.scanData = scanData;
        this.vulnerabilityHandler = vulnerabilityHandler;
    }

    /**
     * Main method to commence parsing the input provided
     *
     * @throws IOException
     */
    public final void parse() throws IOException {
        LOG.info("VulnerabilitiesParser parse starting");
        new ScanDataStreamingJsonParser().expectedStartTokens(JsonToken.START_OBJECT)
                .handler("/queries/*", Query.class, this::handleResult).parse(scanData);
    }

    private void handleResult(Query query) {
        io.secdojo.ssc.parser.kics.domain.File[] files = query.getFiles();
        for (io.secdojo.ssc.parser.kics.domain.File file : files) {
            buildVulnerability(query, file);
        }
    }

    /**
     * Build the vulnerability
     *
     * @param query -  query
     * @param file - file
     */
    private void buildVulnerability(Query query, File file) {
        LOG.info("VulnerabilitiesParser buildVulnerability");
        StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(
                file.getSimilarity_id());

        // Set meta-data
        vb.setEngineType(ENGINE_TYPE);
        vb.setAnalyzer("Kics");

        // Set mandatory values to JavaDoc-recommended values
        vb.setAccuracy(5.0f);
        vb.setConfidence(2.5f);

        // Set standard vulnerability fields based on input
        vb.setFileName(file.getName());
        vb.setPriority(this.getPriority(query));
        vb.setCategory(query.getCategory());
        vb.setLineNumber(file.getLine());
        vb.setSubCategory(file.getIssue_type());
        // Set custom attributes based on input
        vb.setStringCustomAttributeValue(CustomVulnAttribute.Description, query.getDescription());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.QueryName, query.getName());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.QueryUrl, query.getQuery_url());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.Platform, query.getPlatform());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.CloudProvider, query.getCloud_provider());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.FileName, file.getName());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.IssueType, file.getIssue_type());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.ExpectedValue, file.getExpected_value());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.ActualValue, file.getActual_value());

        vb.completeVulnerability();
    }


    private Priority getPriority(Query query) {
        return MAP_SEVERITY_TO_PRIORITY.getOrDefault(query.getSeverity(), Priority.Medium);
    }
}